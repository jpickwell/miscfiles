# miscfiles #

Just some miscellaneous files.

## Schema ##

The schema directory contains schema for various W3C standards.

### Standards ###

 * Extensible Cascading Style Sheets (XCSS) 1
 * Extensible Cascading Style Sheets (XCSS) 2.1
 * Extensible Hypertext Markup Language (XHTML) 1.0
 * Extensible Hypertext Markup Language (XHTML) 1.1
 * Extensible Hypertext Markup Language (XHTML) 2.0
 * Extensible Hypertext Markup Language (XHTML) 5
 * InkML
 * MathML 1.01
 * MathML 2.0
 * MathML 3.0
 * Scalable Vector Graphics (SVG) 1.1
 * Scalable Vector Graphics Basic Profile (SVGB) 1.1
 * Scalable Vector Graphics Tiny Profile (SVGT) 1.2
